<?php

namespace Nanhh\NhatanhComposer\Models;

use Nanhh\NhatanhComposer\Interfaces\Staff;
use Nanhh\NhatanhComposer\Models\Model;

class IT extends Model implements Staff
{
    protected $table = 'it';

    protected $attributes = [
        'id',
        'name',
        'birthday',
        'salary',
        'bonus'
    ];

    public function getSalary()
    {
        return ($this->salary ?: 0) + ($this->bonus ?: 0);
    }
}