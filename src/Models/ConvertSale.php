<?php

namespace Nanhh\NhatanhComposer\Models;

use Nanhh\NhatanhComposer\Interfaces\Staff;
use Nanhh\NhatanhComposer\Models\Model;

class ConvertSale extends Model implements Staff
{
    protected $table = 'cvs';
    protected $attributes = [
        'id',
        'name',
        'birthday',
        'salary',
        'kpi'
    ];
    const KPI = 100;
    public function getSalary()
    {
        if ($this->kpi >= self::KPI) {

            return $this->salary + 15000 * ($this->kpi - self::KPI);
        } else if ($this->kpi >= 80) {

            return $this->salary + 10000 * ($this->kpi - self::KPI);
        } else {
            return $this->salary + 15000 * ($this->kpi - self::KPI);
        }
    }
}