# Composer
- composer là công cụ quản lý thư viện trong php.
- chỉ cần khai báo composer sẽ tự động tải code của các thư viện thông qua sever công cộng
- cài đặt thư viện ở danh mục trong project (vendor)
- composer giúp quản lý thư viện cho từng project riêng biệt

## cài đặt combhpse
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```
- local
```
php composer-setup.php --install-dir=bin --filename=composer

```
- global
```
mv composer.phar /usr/local/bin/composer
``` 
## Cách sử dụng cơ bản
### composer.json
- setup project. để có thể sử  dụng composer trong project cần có file composer.json
- file này mô tả các các dependency dùng trong dự án và những thông tin khác như là version, tên, ...
### thành phần trong composer.json
#### require: 
- khai báo các thư viện và phiên bản project sử dụng
```
{
    "require": {
        "monolog/monolog": "2.0.*"
    }
}
```
#### Package name: 
- gồm vendor name và project name. 
- Package version constraints version mà project hoạt động hiệu quả
- làm sao để composer tải xuống file mình muốn
    - đầu tiên composer sẽ lấy tên package mà ta yêu cầu rồi tìm nó trên repository mà ta đã đăng ký
    - nếu không tìm thấy trong repository đã đăng ký hoặc chưa đăng ký repo thì sẽ tìm ở packagist.org
    - khi tìm được package sẽ tìm tiếp vesion phù hợp nhất mà ta đã khai báo
- Installing dependencies
    - để cài đặt dependencies từ file composer.json ta dùng lệnh update
```
    composer update
```
> note: khi chưa có file composer.lock sử dụng update với install là như nhau
    
- composer update sẽ thực hiện 2 việc:
    1. tìm kiếm toàn bộ dependencies khai báo trong composer.json và version rồi viết vào file composer.lock
    2. chạy ngầm lệnh install. download file dependencies lưu trong thư mục vendor của project
- commit file composer.lock giúp người khác khi setup project biết chính sác version được sử dụng, tránh xung đột
- Installing from composer.lock
```
    composer install
```
    - khi đã có composer.lock lệnh install sẽ cài đặt các dependencies và version cụ thể trong file để tránh sảy ra xung đột
- Updating dependencies to their latest versions
    - conposer.lock lấy chính xác version nên ngăn chặn lấy version mới nhất
    - để lấy version mới nhất dùng lệnh update. lấy version mới nhất trong conposer.json và sửa lại file lock
    - composer sẽ phát cảnh báo nếu install khi conposer.lock chưa lưu lại thay đổi trong composer.json
> nếu chưa có file composer.json dùng lệnh `composer init` để tạo

#### Packagist 
- là kho chính của composer. 
- muốn sử dụng 1 package chỉ cần require package đó. 

#### Autoload
- tự động require thông qua magic method và function
- để sác định thông tin autoload composer tạo ra `file vendor/autoload.php`.
- require `vendor/autoload.php` và sử dụng
- có thể thêm autoload của mình vào `autoload` trong ```composer.json```
- composer sử dụng chuẩn psr4 để sử dụng khi đặt namespace
```
  "autoload": {
        "psr-4": {
            "Nanhh\\NhatanhComposer\\": "src/"
        }
    },
```
- sau khi thêm vao autoload cần chạy lệnh `dump-autoload` lệnh này sẽ tạo lại file `vendor/autoload.php`
- ngoài chuẩn psr-4 composer cũng hỗ trợ chuẩn psr-0
##### psr-0
- psr-0 không cho sử dụng namespace prefix. Nên cần tạo đường dẫn tuyệt đối
```
{
    "autoload": {
        "psr-0": {
            "Controllers\\HomeController": "src/",
            "Models\\User" : "src/"
        }
    }
}

```
##### psr-4
- cho phép sử dụng namespace prefix giúp composer.json gọn hơn
- khi thêm 1 đường dẫn không cần sủa trong composer.json chỉ cần thêm  namespace 
```
\<NamespaceName>(\<SubNamespaceNames>)*\<ClassName>

```
- VD:
```
{
    "autoload": {
        "psr-4": {
            "Viblo\\": "src/"
        }
    }
}
```
-File `HomeController.php`:
```
<?php
namespace Viblo\Controllers;

use Viblo\Models\User;

class HomeController 
{
    public function actionIndex()
    {
        return (new User)->list();
    }
}
```
#### psr-1
- các file phải trong thẻ `<?php` hoặc `<?= `
- dùng utf-8
- namespace phải theo tiêu chuẩn psr-4
- tên class ở định dạng `StudlyCaps`
- hằng số viết in hoa và cách nhau bởi dấu gạch dưới
- phương thức ở định dạng `camelCase`
#### psr-2
- code theo chuẩn psr-1
- dùng 4 phím space để thụt đầu dòng thay vì tab
- sau khi khai báo khối `namespace` phải để 1 dòng trông phía duoi. tương tự với `use`
- ngoặc nhọn cho class phải ở dòng dưới class
- ngoặc nhọn cho phương thức phải ở dòng dưới
- Tính đóng mở(public, protected, private) PHẢI được định nghĩa ở tất cả các thuộc tính và phương thức; abstract và final PHẢI định nghĩa trước tính đóng mở, static PHẢI định nghĩa sau tính đóng mở.
### tài liệu tham khảo
- https://getcomposer.org/doc/01-basic-usage.md
- https://viblo.asia/p/php-autoloading-psr4-and-composer-V3m5Wy0QZO7
- https://viblo.asia/p/psr-2-huong-dan-mau-code-dep-aWj53OpG56m